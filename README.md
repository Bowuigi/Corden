<style>
pre {
	font-size: 16pt;
	font-weight: bold;
}
</style>

# Corden #

**Por Bowuigi**

**Fecha de creación**: 2022-06-07

**Licencia**: CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0)

Sistema de codificación de datos de base 36 con múltiples sets de caracteres.

Se ubican patrones de puntos de igual separación y color que representan diferentes carácteres.

## Número mágico (más bien, coordenadas mágicas) ##

Si se quiere simbolizar que hay un mensaje en Corden en el sistema de coordenadas, se escribe

```
...
```

En la esquina superior izquierda del mensaje (como si fuera un borde) y

```
 .
..
```

En la esquina inferior derecha del mismo de la misma forma, por ejemplo

```
...

  . .
   .
  . .
      .
     ..
```

Esto sirve no solo para identificar que es un mensaje en Corden, sino que tambien indica el color de todos los puntos en el mensaje, su separación y el espacio que ocupa, además facilitando la inserción de múltiples mensajes con diferentes características. De usar líneas entre los puntos, no aplicarlas al código que las identifica.

## Base

Utiliza un sistema de números en base 36 (va del 0 al 9 y luego de A a Z), cada uno se representa con puntos en una grilla o un sistema de coordenadas, a esto se le puede agregar cualquier decoración encima para esconderlo o hacerlo más claro, como líneas o colores.

```
1    2    3    4    5    6    7    8    9    A    B    C
.    .    ..   .    . .  ..   ...  ...  ...   .   ..   ..
.              .     ..   ..  .     ..  ...  .           .
.      .    .   ..    .  ..   ...  ...  ...            


D    E    F    G    H    I    J    K    L    M    N    O
..        ..   . .  .    .     .   . .  .    ...  . .  ..
  .   .   ..   .     ..  .    ..   ..    ..  . .  ...  ..
 .     .  .     .     .            . .                 


P    Q    R    S    T    U    V    W    X    Y    Z
  .  . .  . .  ...  . .  . .  . .  . .  ..   ...  . .
.     .   ..              .   . .  ..   ..    ..  ...
  .    .    .    .   .         .   ..    ..  ..   ..
```

## Conjuntos

Se colocan arriba del número en base 36 y son la clave para hacer que Corden sea extensible y compacto

Solo un conjunto se puede usar al mismo tiempo

Se pueden agregar más conjuntos con frases u otros caracteres de ser necesario (es más, esto es sugerido), solo se necesita compartir las coordenadas del conjunto y el significado de los 36 caracteres en el mismo.

Además, con los conjuntos se pueden transmitir mensajes diferentes a personas diferentes utilizando las mismas coordenadas.

```
Normal         Alternate      Lowercase      AltLower
                .             . .             .
               . .             .             . .
                                              .

Number         Punctuation    Control
 ..            ..             ...
 .              ..            . .
..                            ...
```

<div style="page-break-before: always;">

## Tabla de conjuntos

	+----------+-------------+-------------+------------+----------+----------------+-------------------+
	|  Normal  |  Alternate  |  Lowercase  |  AltLower  |  Number  |  Punctuation   |  Control          |
	+----------+-------------+-------------+------------+----------+----------------+-------------------+
	|  0       |  ⁰          |  ₀          |            |  0       |  *             |  NUL (ASCII 0)    |
	|  1       |  ¹          |  ₁          |            |  1       |  -             |  SOH (ASCII 1)    |
	|  2       |  ²          |  ₂          |            |  2       |  {             |  STX (ASCII 2)    |
	|  3       |  ³          |  ₃          |            |  3       |  }             |  ETX (ASCII 3)    |
	|  4       |  ⁴          |  ₄          |            |  4       |  _             |  EOT (ASCII 4)    |
	|  5       |  ⁵          |  ₅          |            |  5       |  +             |  ENQ (ASCII 5)    |
	|  6       |  ⁶          |  ₆          |            |  6       |  ~             |  ACK (ASCII 6)    |
	|  7       |  ⁷          |  ₇          |            |  7       |  [             |  BEL (ASCII 7)    |
	|  8       |  ⁸          |  ₈          |            |  8       |  ]             |  BS (ASCII 8)     |
	|  9       |  ⁹          |  ₉          |            |  9       |                |  HT (ASCII 9)     |
	|  A       |  Á          |  a          |  á         |  10      |    (ASCII 20)  |  LF (ASCII 10)    |
	|  B       |             |  b          |            |  11      |  .             |  VT (ASCII 11)    |
	|  C       |  Ç          |  c          |  ç         |  12      |  ,             |  FF (ASCII 12)    |
	|  D       |             |  d          |            |  13      |  ;             |  CR (ASCII 13)    |
	|  E       |  É          |  e          |  é         |  14      |  :             |  SO (ASCII 14)    |
	|  F       |             |  f          |            |  15      |  #             |  SI (ASCII 15)    |
	|  G       |             |  g          |            |  16      |  $             |  DLE (ASCII 16)   |
	|  H       |             |  h          |            |  17      |  %             |  DC1 (ASCII 17)   |
	|  I       |  Í          |  i          |  í         |  18      |  '             |  DC2 (ASCII 18)   |
	|  J       |             |  j          |            |  19      |  "             |  DC3 (ASCII 19)   |
	|  K       |             |  k          |            |  20      |  ¿             |  DC4 (ASCII 20)   |
	|  L       |             |  l          |            |  21      |  ?             |  NAK (ASCII 21)   |
	|  M       |             |  m          |            |  22      |  ¡             |  SYN (ASCII 22)   |
	|  N       |  Ñ          |  n          |  ñ         |  23      |  !             |  ETB (ASCII 23)   |
	|  O       |  Ó          |  o          |  ó         |  24      |  @             |  CAN (ASCII 24)   |
	|  P       |             |  p          |            |  25      |  <             |  EM (ASCII 25)    |
	|  Q       |             |  q          |            |  26      |  >             |  SUB (ASCII 26)   |
	|  R       |             |  r          |            |  27      |  &             |  ESC (ASCII 27)   |
	|  S       |             |  s          |            |  28      |  =             |  FS (ASCII 28)    |
	|  T       |             |  t          |            |  29      |  (             |  GS (ASCII 29)    |
	|  U       |  Ú          |  u          |  ú         |  30      |  )             |  RS (ASCII 30)    |
	|  V       |             |  v          |            |  31      |  |             |  US (ASCII 31)    |
	|  W       |             |  w          |            |  32      |  `             |  DEL (ASCII 127)  |
	|  X       |             |  x          |            |  33      |  ^             |                   |
	|  Y       |             |  y          |            |  34      |  /             |                   |
	|  Z       |             |  z          |            |  35      |  \             |                   |
	+----------+-------------+-------------+------------+----------+----------------+-------------------+

## Desventajas

- No es muy eficiente con el espacio: Este nunca fue el objetivo principal ni ninguno de los secundarios, pero se realizó un ligero esfuerzo para mantener cada código de forma legible pero compacta. Si se requiere un sistema más eficiente con el espacio, recomiendo Unicode (específicamente UTF-8) en lugar de Corden.

## ¿Que se puede hacer con Corden?

Corden fue realizado con la intención de ser transmitido por muchas fuentes distintas de diferentes distancias.

+ Dibujos a mano
+ Voz, leyendo las coordenadas una por una o diciendo el conjunto y después el número 0-Z
+ Ondas de sonido, codificando la posición de cada uno según las ondas de diferentes canales de sonido, también aplica para otras ondas como de radio o infrarroja
+ Impresiones
+ Imágenes
+ Videos
+ Modelos 3D
+ Texto plano, coordenadas en formato (x;y) o {x,y} separadas por comas
+ Tablas de datos (y por ende bases de datos) con columnas para las coordenadas X e Y de cada punto.
+ Luz, mostrando cada punto en un orden elegido por el emisor
+ Cualquier cosa que se asemeje a una grilla o a un sistema de coordenadas como funciones matemáticas, tableros de damas, tableros de ajedrez, hojas de cálculo, entre muchos otros.

Corden se puede aprender con facilidad debido a su reducido número de códigos (gracias a los conjuntos).

Corden puede ocultarse con facilidad en imágenes, videos y cualquier otro tipo de gráfico.

Se pueden mostrar varios mensajes en Corden en una misma fuente a la vez, suponiendo que cada uno tiene diferentes características (separación, colores, etc.)

## Formato compacto en binario

Extensión: .cor

Número mágico (Identificador de archivo): COR

Little Endian

Los primeros 4 bits después del número mágico se utilizan para definir cuantos bytes tiene la coordenada X (0-15)

Los siguientes 4 bits se utilizan para definir cuantos bytes tiene la coordenada Y (1-16)

De usar 16 bytes para la coordenada X, los ejes irían de 0 a 340282366920938463463374607431768211455 (aproximadamente 3,403e38), lo mismo para la coordenada Y, esto otorga suficiente espacio para cualquier mensaje.

Ejemplo:

Número 0, conjunto normal, caracter '0', representación gráfica

```
. .
 .
. .
```

Formato en hexadecimal (cada dos números hay un caracter de 1 byte) explicado

```
| 43 4F 52 | COR (Número mágico)          |
| 00       | 1 byte para X, 1 byte para Y |
| 00 00    | Punto en {0,0}               |
| 02 00    | Punto en {2,0}               |
| 01 01    | Punto en {1,1}               |
| 00 02    | Punto en {0,2}               |
| 02 02    | Punto en {2,2}               |
```

## Soporte y extensibilidad

Actualmente Corden soporta todo ASCII y algunas extensiones (el segmento español y portugués de Windows-1252, subscripts y superscripts de Windows-1252 y Unicode), son bienvenidas las extensiones para soportar más caracteres, en especial IBM437 (También conocido como CP437 o Code page 437), Windows-1252 y EBCDIC.

Una de las características importantes de Corden es su extensibilidad, se pueden agregar más conjuntos, no solo con carácteres, sino también con frases.

